CREATE TABLE `auto_nyt`.`auto` (
  `auto_id` int(11) NOT NULL AUTO_INCREMENT,
  `marka` varchar(25) NOT NULL,
  `gyartas_ev` date NOT NULL,
  `szin_kod` varchar(15) NOT NULL,
  PRIMARY KEY (`auto_id`),
  UNIQUE KEY `szin_kod_UNIQUE` (`szin_kod`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1