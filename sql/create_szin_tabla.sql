CREATE TABLE `auto_nyt`.`szin_tabla` (
  `szin_id` INT NOT NULL AUTO_INCREMENT,
  `alap_szin` VARCHAR(15) NOT NULL,
  `szin_kod` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`szin_id`),
  UNIQUE INDEX `szin_kod_UNIQUE` (`szin_kod` ASC));