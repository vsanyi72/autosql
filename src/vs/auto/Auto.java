package vs.auto;

import java.sql.Date;

public class Auto {

	private int autoId;
	private String marka;
	private Date gyartasEv;
	private String szinKod;

	public Auto(int autoId) {
		super();
		this.autoId = autoId;
	}

	public Auto(String marka, Date gyartasEv, String szinKod) {
		super();
		this.marka = marka;
		this.gyartasEv = gyartasEv;
		this.szinKod = szinKod;
	}

	public Auto(int autoId, String marka, Date gyartasEv, String szinKod) {
		super();
		this.autoId = autoId;
		this.marka = marka;
		this.gyartasEv = gyartasEv;
		this.szinKod = szinKod;
	}

	public int getAutoId() {
		return autoId;
	}

	public void setAutoId(int autoId) {
		this.autoId = autoId;
	}

	public String getMarka() {
		return marka;
	}

	public void setMarka(String marka) {
		this.marka = marka;
	}

	public Date getGyartasEv() {
		return gyartasEv;
	}

	public void setGyartasEv(Date gyartasEv) {
		this.gyartasEv = gyartasEv;
	}

	public String getSzinKod() {
		return szinKod;
	}

	public void setSzinKod(String szinKod) {
		this.szinKod = szinKod;
	}

	@Override
	public String toString() {
		return new StringBuilder()
				.append("ID : ").append(this.getAutoId())
				.append(", Marka : ").append(this.getMarka())
				.append(", Gyartas eve : ").append(this.getGyartasEv())
				.append(", Szin kod : ").append(this.getSzinKod()).toString();
	}

}
