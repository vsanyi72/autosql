package vs.auto;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AutoDao implements Dao<Auto> {

	private Connection connection;

	public AutoDao(Connection connection) {
		super();
		this.connection = connection;
	}

	@Override
	public Auto read(int id) throws SQLException {
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM auto WHERE auto_id = ?");
		preparedStatement.setInt(1, id);
		ResultSet result = preparedStatement.executeQuery();

		int autoId;
		String marka;
		Date gyartasEv;
		String szinKod;

		Auto auto = null;

		while (result.next()) {
			autoId = result.getInt("auto_id");
			marka = result.getString("marka");
			gyartasEv = result.getDate("gyartas_ev");
			szinKod = result.getString("szin_kod");
			auto = new Auto(autoId, marka, gyartasEv, szinKod);
		}

		return auto;
	}

	@Override
	public void create(Auto entity) throws SQLException {
		PreparedStatement preparedStatement = this.connection
				.prepareStatement("INSERT INTO auto (marka, gyartas_ev, szin_kod) VALUES(?,?,?)");
		preparedStatement.setString(1, entity.getMarka());
		preparedStatement.setDate(2, entity.getGyartasEv());
		preparedStatement.setString(3, entity.getSzinKod());
		preparedStatement.executeUpdate();
	}

	@Override
	public void update(Auto entity) throws SQLException {
		PreparedStatement preparedStatement = this.connection
				.prepareStatement("UPDATE auto SET marka=?, gyartas_ev=?, szin_kod=? WHERE auto_id=?");
		preparedStatement.setString(1, entity.getMarka());
		preparedStatement.setDate(2, entity.getGyartasEv());
		preparedStatement.setString(3, entity.getSzinKod());
		preparedStatement.setInt(4, entity.getAutoId());
		preparedStatement.executeUpdate();

	}

	@Override
	public void delete(Auto entity) throws SQLException {
		PreparedStatement preparedStatement = this.connection.prepareStatement("DELETE FROM auto WHERE auto_id=?");
		preparedStatement.setInt(1, entity.getAutoId());
		preparedStatement.executeUpdate();

	}

}
