package vs.auto;

public class Szin {



	private int szinId;
	private String alapSzin;
	private String szinKod;

	public Szin(int szinId) {
		super();
		this.szinId = szinId;
	}


	public Szin(String alapSzin, String szinKod) {
		super();
		this.alapSzin = alapSzin;
		this.szinKod = szinKod;
	}

	public Szin(int szinId, String alapSzin, String szinKod) {
		super();
		this.szinId = szinId;
		this.alapSzin = alapSzin;
		this.szinKod = szinKod;
	}

	
	public int getSzinId() {
		return szinId;
	}

	public void setSzinId(int szinId) {
		this.szinId = szinId;
	}

	public String getAlapSzin() {
		return alapSzin;
	}

	public void setAlapSzin(String alapSzin) {
		this.alapSzin = alapSzin;
	}

	public String getSzinKod() {
		return szinKod;
	}

	public void setSzinKod(String szinKod) {
		this.szinKod = szinKod;
	}


	@Override
	public String toString() {
		return new StringBuilder().append("Szin ID : ").append(this.getSzinId())
				.append(", Alapszin : ").append(this.getAlapSzin())
				.append(", Szin kod : ").append(this.getSzinKod()).toString();
	}
}
