package vs.auto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;



public class SzinDao implements Dao<Szin> {

	Connection connection;

	public SzinDao(Connection connection) {
		super();
		this.connection = connection;
	}

	@Override
	public Szin read(int id) throws SQLException {
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM szin_tabla WHERE szin_id=?");
		preparedStatement.setInt(1, id);
		ResultSet result = preparedStatement.executeQuery();

		int szinId;
		String alapSzin;
		String szinKod;
		Szin szin = null;
		while (result.next()) {
			szinId = result.getInt("szin_id");
			alapSzin = result.getString("alap_szin");
			szinKod = result.getString("szin_kod");
			szin = new Szin(szinId, alapSzin, szinKod);
		}
		return szin;
	}

	@Override
	public void create(Szin entity) throws SQLException {
		PreparedStatement preparedStatement = this.connection.prepareStatement
				("INSERT INTO szin_tabla (alap_szin, szin_kod) VALUES (?,?)");		
		preparedStatement.setString(1, entity.getAlapSzin());
		preparedStatement.setString(2, entity.getSzinKod());
		preparedStatement.executeUpdate();
	
	}

	@Override
	public void update(Szin entity) throws SQLException {
		PreparedStatement preparedStatement = this.connection.prepareStatement
				("UPDATE szin_tabla SET alap_szin=?, szin_kod=? WHERE szin_id=?");
		preparedStatement.setString(1, entity.getAlapSzin());
		preparedStatement.setString(2, entity.getSzinKod());
		preparedStatement.setInt(3, entity.getSzinId());
		preparedStatement.executeUpdate();
	}

	@Override
	public void delete(Szin entity) throws SQLException {
		PreparedStatement preparedStatement = this.connection.prepareStatement
				("DELETE FROM szin_tabla WHERE szin_id=?");
		preparedStatement.setInt(1, entity.getSzinId());
		preparedStatement.executeUpdate();

	}

}
